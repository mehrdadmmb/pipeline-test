package com.mmb.myapplication

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.mmb.myapplication.ui.theme.MyApplicationTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.apache.commons.math3.ml.clustering.Cluster
import org.apache.commons.math3.ml.clustering.DoublePoint
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

class MainActivity : ComponentActivity() {
    companion object {
        const val PERMISSION_REQUEST_CODE = 1
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    Greeting(context = this, text = NetworkConfig.BASE_URL, callback = {
                        Toast.makeText(this@MainActivity, NetworkConfig.BASE_URL, Toast.LENGTH_LONG).show()
                    })
                }
            }
        }


//        cleanLocation()


//            requestPermission()

    }

    fun cleanLocation() {
        val locations = listOf(
            doubleArrayOf(35.6895, 51.3890),
            doubleArrayOf(32.4279, 53.6880),
            doubleArrayOf(37.2777383, 49.596998),
            doubleArrayOf(37.2777382, 49.5970116),
            doubleArrayOf(37.277754, 49.5969238),
            doubleArrayOf(37.2777617, 49.5971607),
            doubleArrayOf(37.2773804, 49.596987),
            doubleArrayOf(37.2772915, 49.596123),
            doubleArrayOf(37.2770871, 49.5956223),
            doubleArrayOf(37.2769045, 49.5940624),
            doubleArrayOf(37.2769594, 49.5934808),
            doubleArrayOf(37.2784456, 49.5931913),
            doubleArrayOf(37.2786946, 49.5930667),
            doubleArrayOf(37.2809827, 49.5928441),
            doubleArrayOf(37.2829781, 49.5917024),
            doubleArrayOf(37.2838699, 49.5918959),
            doubleArrayOf(37.283937, 49.5931684),
            doubleArrayOf(37.2864958, 49.5938031),
            doubleArrayOf(37.2893079, 49.5937552),
            doubleArrayOf(37.2901597, 49.5919691),
            doubleArrayOf(37.2903963, 49.5909276),
            doubleArrayOf(37.290504, 49.5910132),
            doubleArrayOf(37.2907514, 49.5904984),
            doubleArrayOf(37.291053, 49.5903521),
            doubleArrayOf(37.2931364, 49.5902467),
            doubleArrayOf(37.2944989, 49.5894885),
            doubleArrayOf(37.2964299, 49.5896723),
            doubleArrayOf(37.2971427, 49.5894313),
            doubleArrayOf(37.2972386, 49.5896255),
            doubleArrayOf(37.2974082, 49.5885392),
            doubleArrayOf(37.2973025, 49.5890845),
            doubleArrayOf(37.2969597, 49.5889715),
            doubleArrayOf(37.2969597, 49.5889715),
            doubleArrayOf(37.2971153, 49.5888485),
            doubleArrayOf(37.2972316, 49.5889028),
            doubleArrayOf(37.2974601, 49.5886037),
            doubleArrayOf(37.2974587, 49.5886191),
            doubleArrayOf(37.2973708, 49.588714),
            doubleArrayOf(37.2973862, 49.5886875),
            doubleArrayOf(37.2973492, 49.588622),
            doubleArrayOf(37.2972891, 49.5886131),
            doubleArrayOf(37.297372, 49.5886254),
            doubleArrayOf(37.2974186, 49.5886521),
            doubleArrayOf(37.2973792, 49.5886447),
            doubleArrayOf(37.2974009, 49.5886546),
            doubleArrayOf(37.297432, 49.5886373),
            doubleArrayOf(37.2970512, 49.5882718),
            doubleArrayOf(37.2973234, 49.5886545),
            doubleArrayOf(37.297403, 49.5886363),
            doubleArrayOf(37.2973469, 49.5887276),
            doubleArrayOf(37.297415, 49.5886743),
            doubleArrayOf(37.2971761, 49.5884559),
            doubleArrayOf(37.2971761, 49.5884559),
            doubleArrayOf(37.2973879, 49.5886323),
            doubleArrayOf(37.2971966, 49.5889342),
            doubleArrayOf(37.2971222, 49.5890373),
            doubleArrayOf(37.2974251, 49.5898198),
            doubleArrayOf(37.2977069, 49.5894979),
            doubleArrayOf(37.2975814, 49.5892638),
            doubleArrayOf(37.2975814, 49.5892638),
            doubleArrayOf(37.2981045, 49.5894431),
            doubleArrayOf(37.2975777, 49.5893645),
            doubleArrayOf(37.2989609, 49.5896043),
            doubleArrayOf(37.2991055, 49.5895277),
            doubleArrayOf(37.2993563, 49.5895811),
            doubleArrayOf(37.2997225, 49.5894796),
            doubleArrayOf(37.2994476, 49.5892124),
            doubleArrayOf(37.3001025, 49.5892901),
            doubleArrayOf(37.3002874, 49.58931),
            doubleArrayOf(37.3002869, 49.5891962),
            doubleArrayOf(37.3003749, 49.5891466),
            doubleArrayOf(37.3004152, 49.5891255),
            doubleArrayOf(37.3004272, 49.5891839),
            doubleArrayOf(37.3002581, 49.5893743),
            doubleArrayOf(37.3002991, 49.5893826),
            doubleArrayOf(37.2998085, 49.5893566),
            doubleArrayOf(37.3000773, 49.588824),
            doubleArrayOf(37.299487, 49.5885769),
            doubleArrayOf(37.2997365, 49.5888363),
            doubleArrayOf(37.2997365, 49.5888363),
            doubleArrayOf(37.2984692, 49.5884959),
            doubleArrayOf(37.2996613, 49.5890538),
            doubleArrayOf(37.2996613, 49.5890538),
            doubleArrayOf(37.2987175, 49.5887928),
            doubleArrayOf(37.2977498, 49.5885946),
            doubleArrayOf(37.2994485, 49.5890717),
            doubleArrayOf(37.2994486, 49.589072),
            doubleArrayOf(37.2974716, 49.5887599),
            doubleArrayOf(37.2966557, 49.5884581),
            doubleArrayOf(37.296513, 49.5884356),
            doubleArrayOf(37.2964512, 49.5880728),
            doubleArrayOf(37.2963739, 49.5872171),
            doubleArrayOf(37.296135, 49.5872631),
            doubleArrayOf(37.2959691, 49.5873166),
            doubleArrayOf(37.2957318, 49.5878533),
            doubleArrayOf(37.2956811, 49.5878597),
            doubleArrayOf(37.2957243, 49.588124),
            doubleArrayOf(37.2956339, 49.5884767),
            doubleArrayOf(37.2956556, 49.5886249),
            doubleArrayOf(37.2956457, 49.589145),
            doubleArrayOf(37.295619, 49.5894079),
            doubleArrayOf(37.2954986, 49.5895963),
            doubleArrayOf(37.295322, 49.5895633),
            doubleArrayOf(37.2951519, 49.5896067),
            doubleArrayOf(37.294951, 49.5896544),
            doubleArrayOf(37.2950775, 49.5896666),
            doubleArrayOf(37.2952652, 49.5897038),
            doubleArrayOf(37.2954747, 49.5897307),
            doubleArrayOf(37.2957185, 49.589664),
            doubleArrayOf(37.2961003, 49.5895527),
            doubleArrayOf(37.2962193, 49.589862),
            doubleArrayOf(37.2962295, 49.5898587),
            doubleArrayOf(37.2963, 49.5903182),
            doubleArrayOf(37.2965106, 49.5896541),
            doubleArrayOf(37.2965642, 49.5894619),
            doubleArrayOf(37.2962416, 49.5895133),
            doubleArrayOf(37.296915, 49.5893794),
            doubleArrayOf(37.2966315, 49.5895154),
            doubleArrayOf(37.2970474, 49.5892477),
            doubleArrayOf(37.2965878, 49.5894365),
            doubleArrayOf(37.2973835, 49.5888043),
            doubleArrayOf(37.2974025, 49.588655),
            doubleArrayOf(37.2974516, 49.5886757),
            doubleArrayOf(37.2974631, 49.5886491),
            doubleArrayOf(37.297451, 49.5886322),
            doubleArrayOf(37.2973885, 49.5887067),
            doubleArrayOf(37.2971858, 49.5886895),
            doubleArrayOf(37.2971376, 49.5889132),
            doubleArrayOf(37.2966722, 49.5890471),
            doubleArrayOf(37.2966713, 49.5890471),
            doubleArrayOf(37.2966713, 49.5890471),
            doubleArrayOf(37.2970511, 49.5889394),
            doubleArrayOf(37.296756, 49.5889697),
            doubleArrayOf(37.2973138, 49.589092),
            doubleArrayOf(37.2972075, 49.5888235),
            doubleArrayOf(37.2970294, 49.5890063),
            doubleArrayOf(37.2972322, 49.5889332),
            doubleArrayOf(37.2972828, 49.5888841),
            doubleArrayOf(37.2969627, 49.5888069),
            doubleArrayOf(37.2972005, 49.5888156),
            doubleArrayOf(37.2970148, 49.5886416),
            doubleArrayOf(37.2972479, 49.5888319),
            doubleArrayOf(37.2972834, 49.5888415),
            doubleArrayOf(37.2972834, 49.5888415),
            doubleArrayOf(37.2972219, 49.5888674),
            doubleArrayOf(37.2967815, 49.5886648),
            doubleArrayOf(37.2967815, 49.5886648),
            doubleArrayOf(37.2971042, 49.5886865),
            doubleArrayOf(37.2972764, 49.5887711),
            doubleArrayOf(37.2971901, 49.588726),
            doubleArrayOf(37.2972604, 49.5887871),
            doubleArrayOf(37.2972246, 49.5887188),
            doubleArrayOf(37.2972723, 49.5887988),
            doubleArrayOf(37.2972132, 49.5888831),
            doubleArrayOf(37.2971375, 49.5884674),
            doubleArrayOf(37.2971808, 49.5888965),
            doubleArrayOf(37.2971603, 49.5889335),
            doubleArrayOf(37.2972205, 49.5889294),
            doubleArrayOf(37.2972055, 49.5888281),
            doubleArrayOf(37.2972257, 49.5888456),
            doubleArrayOf(37.2972498, 49.5888333),
            doubleArrayOf(37.297192, 49.5888767),
            doubleArrayOf(37.2968146, 49.5886451),
            doubleArrayOf(37.2974548, 49.5886468),
            doubleArrayOf(37.2974205, 49.5887182),
            doubleArrayOf(37.2972562, 49.5888132),
            doubleArrayOf(37.2971544, 49.588773),
            doubleArrayOf(37.2971991, 49.5889017),
            doubleArrayOf(37.2969081, 49.5885826),
            doubleArrayOf(37.2972051, 49.5888925),
            doubleArrayOf(37.2972022, 49.588796),
            doubleArrayOf(37.2971142, 49.5889064),
            doubleArrayOf(37.2971905, 49.5889041),
            doubleArrayOf(37.2971324, 49.5887268),
            doubleArrayOf(37.2971667, 49.5888838),
            doubleArrayOf(37.2972388, 49.5889118),
            doubleArrayOf(37.2972124, 49.5888592),
            doubleArrayOf(37.29726, 49.5888791),
            doubleArrayOf(37.2972756, 49.5886188),
            doubleArrayOf(37.2971828, 49.5887515),
            doubleArrayOf(37.2972797, 49.5888839),
            doubleArrayOf(37.2972409, 49.5888659),
            doubleArrayOf(37.2972742, 49.5887653),
            doubleArrayOf(37.2971895, 49.5888428),
            doubleArrayOf(37.297165, 49.5888372),
            doubleArrayOf(37.2971175, 49.5887276),
            doubleArrayOf(37.2971152, 49.5887051),
            doubleArrayOf(37.2970518, 49.5887194),
            doubleArrayOf(37.2971919, 49.5889017),
            doubleArrayOf(37.2972869, 49.5887541),
            doubleArrayOf(37.2972025, 49.5888296),
            doubleArrayOf(37.2972417, 49.5888735),
            doubleArrayOf(37.2971333, 49.5886913),
            doubleArrayOf(37.2972241, 49.5888296),
            doubleArrayOf(37.2970156, 49.5886115),
            doubleArrayOf(37.2971678, 49.5887578),
            doubleArrayOf(37.2971718, 49.5888554),
            doubleArrayOf(37.2972112, 49.5888937),
            doubleArrayOf(37.2971361, 49.5887963),
            doubleArrayOf(37.2973925, 49.5887238),
            doubleArrayOf(37.2974396, 49.5886309),
            doubleArrayOf(37.2974722, 49.5886065),
            doubleArrayOf(37.2974521, 49.5886268),
            doubleArrayOf(37.2973932, 49.5886502),
            doubleArrayOf(37.2973822, 49.5887435),
            doubleArrayOf(37.2973822, 49.5887435),
            doubleArrayOf(37.2973739, 49.5886697),
            doubleArrayOf(37.2973004, 49.5886741),
            doubleArrayOf(37.2974403, 49.5886413),
            doubleArrayOf(37.2971711, 49.588695),
            doubleArrayOf(37.2974656, 49.5882091),
            doubleArrayOf(37.2972221, 49.5885435),
            doubleArrayOf(37.2995627, 49.5835114),
            doubleArrayOf(37.3004955, 49.5825984),
            doubleArrayOf(37.3017593, 49.5813363),
            doubleArrayOf(37.3044213, 49.5821152),
            doubleArrayOf(37.3049958, 49.5772229),
            doubleArrayOf(37.3073545, 49.5750701),
            doubleArrayOf(37.3093132, 49.5734297),
            doubleArrayOf(37.3115807, 49.5721155),
            doubleArrayOf(37.3144599, 49.5712643),
            doubleArrayOf(37.3150973, 49.5710581),
            doubleArrayOf(37.3151015, 49.5710007),
            doubleArrayOf(37.3151197, 49.5707174),
            doubleArrayOf(37.3151195, 49.5707135),
            doubleArrayOf(37.3151186, 49.5707205),
            doubleArrayOf(37.3137948, 49.5730662),
            doubleArrayOf(37.3139214, 49.5735437),
            doubleArrayOf(37.3130328, 49.5737001),
            doubleArrayOf(37.3126186, 49.573975),
            doubleArrayOf(37.3125927, 49.5752012),
            doubleArrayOf(37.311816, 49.5764758),
            doubleArrayOf(37.3109297, 49.5751982),
            doubleArrayOf(37.3103495, 49.5737599),
            doubleArrayOf(37.3103354, 49.5738712),
            doubleArrayOf(37.3103961, 49.5737194),
            doubleArrayOf(37.310466, 49.5739894),
            doubleArrayOf(37.3104934, 49.5739685),
            doubleArrayOf(37.3102983, 49.5738908),
            doubleArrayOf(37.3107125, 49.5740243),
            doubleArrayOf(37.3103374, 49.5739838),
            doubleArrayOf(37.3103648, 49.5739233),
            doubleArrayOf(37.3103648, 49.5739233),
            doubleArrayOf(37.3104175, 49.5739564),
            doubleArrayOf(37.3103028, 49.57394),
            doubleArrayOf(37.3103731, 49.5738011),
            doubleArrayOf(37.3102852, 49.5740109),
            doubleArrayOf(37.3102906, 49.5738027),
            doubleArrayOf(37.3104615, 49.5739848),
            doubleArrayOf(37.3103664, 49.5736016),
            doubleArrayOf(37.3102368, 49.5737697),
            doubleArrayOf(37.3103842, 49.5739413),
            doubleArrayOf(37.3103806, 49.5739051),
            doubleArrayOf(37.3104396, 49.5737666),
            doubleArrayOf(37.3104396, 49.5737666),
            doubleArrayOf(37.310154, 49.5739063),
            doubleArrayOf(37.310252, 49.5736213),
            doubleArrayOf(37.3101601, 49.573829),
            doubleArrayOf(37.3102368, 49.5738927),
            doubleArrayOf(37.3101738, 49.5736671),
            doubleArrayOf(37.3104787, 49.5739729),
            doubleArrayOf(37.3103971, 49.5736335),
            doubleArrayOf(37.310409, 49.5739305),
            doubleArrayOf(37.3103256, 49.5738488),
            doubleArrayOf(37.310306, 49.5738981),

            )

        // تعداد خوشه‌ها
        val numClusters = 70

        // ایجاد یک نمونه از الگوریتم K-Means
        val kMeans = KMeansPlusPlusClusterer<DoublePoint>(numClusters)

        // اجرای الگوریتم بر روی داده‌ها
        val clusters: List<Cluster<DoublePoint>> = kMeans.cluster(locations.map { DoublePoint(it) })

        // نمایش نتایج
        for (i in clusters.indices) {
//            println("خوشه $i:")
            for (point in clusters[i].points) {
                println("(${point.point[0]}, ${point.point[1]})")
            }
//            Log.d("mehrdad", "cluster")
        }

    }

    private fun startSignalRHub() {
        CoroutineScope(Dispatchers.IO).launch {
            try {

                SignalRHelper.getHub()?.start()?.doOnDispose {
                    Log.w("signalr", "doOnDispose")
                }?.doOnTerminate {
                    Log.w("signalr", "doOnTerminate")
//                    ConstKeeper.isConnectionClosed = true
                }?.doOnSubscribe {
                    Log.w("signalr", "doOnSubscribe")

                    try {
                        Log.i("signalr", SignalRHelper.getHub().connectionId.toString())
                    } catch (e: Exception) {
                    }
                    SignalRHelper.getHub().on("signalrReceiveMessage", { name: String ->
                        Log.i("signalr", name)
                    }, String::class.java)

//                    ConstKeeper.isConnectionClosed = false
                }?.doOnError {
                    Log.w("signalr", "doOnError ${it.message}")
                }?.blockingAwait()

                SignalRHelper.getHub()?.keepAliveInterval = 2000
                SignalRHelper.getHub()?.serverTimeout = 30000
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                Log.e("signalr", "Exception ${e.message}")

            }
        }
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (Environment.isExternalStorageManager()) {
                startActivity(Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION))
            }
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE),
                PERMISSION_REQUEST_CODE
            )
        }
    }

    private fun checkPermission(): Boolean {
        val permission = ContextCompat.checkSelfPermission(
            this,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        return permission == PackageManager.PERMISSION_GRANTED
    }

}


fun readTextFromFile(context: Context, folderName: String, fileName: String): String? {
    // مسیر فولدر در حافظه خارجی
    val fileDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), folderName)
    if (!fileDir.exists()) {
        return null // فولدر وجود ندارد
    }

    // فایل متنی
    val textFile = File(fileDir, fileName)
    return try {
        FileInputStream(textFile).use { input ->
            input.bufferedReader().use { it.readText() }
        }
    } catch (e: IOException) {
        e.printStackTrace()
        null
    }
}

fun saveTextToFile(context: Context, folderName: String, fileName: String, textContent: String) {
    // ایجاد مسیر فولدر در حافظه خارجی
//    val fileDir = File(context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), folderName)
//    val publicDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)

    val fileDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), folderName)
    if (!fileDir.exists()) {
        fileDir.mkdirs() // ایجاد فولدر اگر وجود ندارد
    }

    // ایجاد فایل متنی
    val textFile = File(fileDir, fileName)
    try {
        FileOutputStream(textFile).use { output ->
            output.write(textContent.toByteArray())
        }
    } catch (e: IOException) {
        e.printStackTrace()
    }
}


@Composable
fun Greeting(context: Context, text: String, callback: () -> Unit) {
    Column(modifier = Modifier) {
        Text(text = text)
        Spacer(modifier = Modifier.height(50.dp))
        FilledTonalButton(onClick = {

            callback()
//            saveTextToFile(context, "MyFolder", "example.txt", "متنی که می‌خواهید ذخیره کنید")

        }) {
            Text(
                text = "استارت",

                )
        }
        FilledTonalButton(onClick = {
            val fileContent = readTextFromFile(context, "MyFolder", "example.txt")
            fileContent?.let {
                // محتوای فایل خوانده شده
                println(it)
            }
        }) {
            Text(
                text = "خواندن",

                )
        }
    }
}
