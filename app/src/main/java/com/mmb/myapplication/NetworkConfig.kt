package com.mmb.myapplication

object NetworkConfig {
    val BASE_URL: String = if (BuildConfig.BASE_URL!=null || BuildConfig.BASE_URL=="null")  "https://debug.example.com/api" else BuildConfig.BASE_URL
}