package com.mmb.myapplication;


import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;


public class SignalRHelper {
    public static HubConnection hubConnection;


    public static HubConnection getHub() {
        if (hubConnection == null) {
            hubConnection = HubConnectionBuilder.create("http://172.24.68.147:7000/contact")
                    .withHeader("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjY1MjI4RTFCQUI3OTEzNzVCN0I4NzFGMDdFMzE0MkI4NTI0MkU5NzYiLCJ4NXQiOiJaU0tPRzZ0NUUzVzN1SEh3ZmpGQ3VGSkM2WFkiLCJ0eXAiOiJhdCtqd3QifQ.eyJzdWIiOiJhM2RlYWMyNC1kNjc3LTQ3YTYtYWI3OS1lNmE1M2Y4MGU1NTEiLCJuYW1lIjoiMDkwNTExMTExMTEiLCJvaV9wcnN0IjoiYW5ndWxhciIsImNsaWVudF9pZCI6ImFuZ3VsYXIiLCJvaV90a25faWQiOiJhZmUzOWExZi1lMmEyLTQxYjktYTNjNy1jNWQ4ODc4YTBkODEiLCJhdWQiOiJoYXN0aV9zZXJ2ZXIiLCJzY29wZSI6Imhhc3RpX2FwaSIsImV4cCI6MTcxNjc5MDU1MSwiaXNzIjoiaHR0cHM6Ly9kZXYtaWRwLnphcC1leHByZXNzLmNvbS8iLCJpYXQiOjE3MTYxOTA1NTF9.Rj90_HZoDoydrzVUuviqW8AUQoQo2RRNWx_fFFAjl1T_S6okihlAasz3DMXuz14hj0RwvpgcmP61JGJ6XjG3F_TRDx8XqFjo5MJHMid9DSE_1qwth-9OGL7sfAZaHQkhzFr63T6-6_qvszYKzHs-FOx129k6J81nPDLneHYh5tu2JHViJFvSTGISB8aRxGa47eL8n2c-LRHNx2Hcnr6Hk64zhVkXauPKu8eKFb8YKZ7W_1knBpWxWRtrtHITCH0xe6FwYCSoWsVCSlVcIs1K5u3VmXZnrr2x8V2GmQjDMXe_qIus_rB1d5Tx3Gkw0K1HiBK5rl5B6Zh6CPhSXe_MAQ").build();
        }
        return hubConnection;
    }

    public static void setNull() {
        SignalRHelper.getHub().stop();
        hubConnection = null;
    }
}
